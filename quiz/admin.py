from django.contrib import admin

# Register your models here.
from .models import RequestQues, Questions

admin.site.register(Questions)
admin.site.register(RequestQues)