from django.contrib import admin
from django.urls import path
# from LearnAJAX import views as a
# from Quiz import views
from .views import welcom, quiz, show, RequestList, RequestComplete, RequestDelete
from django.conf import settings
from django.conf.urls.static import static

app_name = 'quiz'


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcom, name='welcom'),
    path('quiz/', quiz, name='quiz'),
    # path('result/', result, name='result'),
    path('showAnswer/', show, name='show'),
    path('request/', RequestList.as_view(), name='req_list_url'),
    path('request/<str:id>/completed/', RequestComplete.as_view(), name="req_complete_url"),
    path('request/<str:id>/delete/', RequestDelete.as_view(), name="req_delete_url"),
    ]