from django.shortcuts import render, redirect
from .models import RequestQues, Questions
from .forms import FormQuestion, RequestForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, auth
from django.views.generic import View
from django.http import JsonResponse
from django.forms.models import model_to_dict
# Create your views here.

def welcom(request):
    return render(request, 'quiz/welcom.html')

@login_required
def quiz(request):
    return render(request, 'quiz/quiz.html')

@login_required
def show(request):
    ques = Questions.objects.all()
    return render(request, 'quiz/show.html', {'ques':ques})


class RequestList(View):
    # @login_required
    def get(self, request):
        form = RequestForm()
        req = RequestQues.objects.all()
        return render(request, 'quiz/request.html', context={'form': form, 'req':req})

    # @login_required
    def post(self, request):
        form = RequestForm(request.POST)
        if form.is_valid():
            new_ques = form.save()
            return JsonResponse({'ques': model_to_dict(new_ques)}, status=200)
        else:
            return redirect('quiz:req_list_url')


class RequestComplete(View):
    def post(self, request, id):
        ques = RequestQues.objects.get(id=id)
        ques.completed = True
        ques.save()
        return JsonResponse({'ques': model_to_dict(ques)}, status=200)


class RequestDelete(View):
    def post(self, request, id):
        ques = RequestQues.objects.get(id=id)
        ques.delete()
        return JsonResponse({'result': 'ok'}, status=200)