from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

# Create your models here.

class RequestQues(models.Model):
    title = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    completed = models.BooleanField(default=False);

    def __str__(self):
        return self.title;

    class Meta:
        ordering = ['completed', 'date']

class Questions(models.Model):
    question = models.CharField(max_length=300, null=True)
    option1 = models.CharField(max_length=200)
    option2 = models.CharField(max_length=200)
    option3 = models.CharField(max_length=200)
    option4 = models.CharField(max_length=200)
    answer = models.CharField(max_length=200)
