$(document).ready(function(){

    var csrfToken = $("input[name=csrfmiddlewaretoken]").val();

    $("#addButton").click(function(){
        var serializedData = $("#addQuesForm").serialize();
        // console.log(serializedData)

        $.ajax({
            url : $("addQuesForm").data('url'),
            data: serializedData,
            type: 'post',
            success: function(response) {
                $("#quesList").append('<div class="card mb-1" id="taskCard" data-id="' + response.ques.id + '"><div class="card-body">'+ 
                response.ques.title +
                '<button type="button" class="close float-right" data-id="' + response.ques.id +'"><span aria-hidden="true">&times;</span></button></div></div>')
            }
        })

        $("#addQuesForm")[0].reset(); //supaya kalo abis nge add, di inputannya hilang

    });

    $("#quesList").on('click', '.card', function(){
        var dataId = $(this).data('id');
        console.log(dataId)
        $.ajax({
            url: '/quiz/request/' + dataId + '/completed/',
            data: {
                csrfmiddlewaretoken: csrfToken,
                id: dataId
            },
            type: 'post',
            success: function(){
                var cardItem = $('#quesCard[data-id="' + dataId + '"]');
                cardItem.css('text-decoration', 'line-trough').hide().slideDown();
                $('#quesList').append(cardItem);
            }
        });
    }).on('click', 'button.close', function(event) {
        event.stopPropagation();

        var dataId = $(this).data('id');

        $.ajax({
            url: '/quiz/request/' + dataId + '/delete/',
            data: {
                csrfmiddlewaretoken: csrfToken,
                id: dataId
            },
            type: 'post',
            dataType: 'json',
            success: function(){
                $('#quesCard[data-id="' + dataId + '"]').remove();
            }
        })
    });
});