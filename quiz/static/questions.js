// bikin array 
let questions = [
    {
    numb: 1,
    question: "Kata ‘Corona’ berasal dari bahasa Latin yang memiliki arti?",
    answer: "Mahkota",
    options: [
      "Mahkota",
      "Dermaga",
      "Virus",
      "Bunga"
    ]
  },
    {
    numb: 2,
    question: "Diwajibkan untuk menggunakan masker, saat...",
    answer: "Pergi keluar rumah.",
    options: [
      "Mandi.",
      "Pergi keluar rumah.",
      "Tidur.",
      "Pembelajaran dirumah."
    ]
  },
    {
    numb: 3,
    question: "Ketika menjalani pemeriksaan di pelayanan kesehatan, petugas kesehatan akan melakukan...",
    answer: "Screening",
    options: [
      "Screening",
      "Streaming",
      "Dancing",
      "Parking"
    ]
  },
    {
    numb: 4,
    question: "Masyarakat bisa mengakses layanan ... untuk laporan gawat darurat masalah kesehatan.",
    answer: "119",
    options: [
      "911",
      "081",
      "119",
      "021"
    ]
  },
    {
    numb: 5,
    question: "Tiga jenis masker yang direkomendasikan oleh Kementerian Kesehatan, yakni, kecuali.",
    answer: "Masker buff",
    options: [
      "Masker bedah",
      "Masker kain",
      "Masker buff",
      "Masker N95"
    ]
  },
];
