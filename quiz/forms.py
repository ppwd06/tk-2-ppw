from django import forms
from .models import RequestQues
from django.contrib.auth.models import User

ans1 = [
    ('mahkota', 'Mahkota'),
    ('dermaga', 'Dermaga'),
    ('virus', 'Virus'),
    ('bunga', 'Bunga'),
]

ans2 = [
    ('a', 'Mencuci tangan secara rutin.'),
    ('b', 'Selalu jaga jarak aman dengan orang yang batuk atau bersin.'),
    ('c', 'Jangan sentuh mata, hidung, atau mulut Anda.'),
    ('d', 'Jalan-jalan ke mall karena bosan di rumah saja.'),
]

ans3 = [
    ('a', 'Mandi.'),
    ('b', 'Pergi keluar rumah.'),
    ('c', 'Tidur'),
    ('d', 'Pembelajaran dirumah.'),
]

ans4 = [
    ('a', 'Gunakan masker.'),
    ('b', 'Patuhi etika batuk/bersin yang benar. Caranya, menutup mulut dan hidung dengan tisu atau punggung lengan.'),
    ('c', 'Mengenakan APD(alat pelindung diri) dan payung.'),
    ('d', 'Upayakan tidak menggunakan transportasi massal.'),
]

ans5 = [
    ('a', 'Akan dirujuk ke salah satu rumah sakit (RS) rujukan yang siap untuk penanganan Covid-19.'),
    ('b', 'Akan diantar ke RS rujukan menggunakan ambulans didampingi oleh nakes yang menggunakan alat pelindung diri (APD).'),
    ('c', 'Di RS rujukan akan dilakukan pengambilan spesimen untuk pemeriksaan laboratorium dan dirawat di ruang isolasi.'),
    ('d', 'Akan diperintahkan oleh nakes untuk pergi berlibur.')
]

ans6 = [
    ('a', 'Screening'),
    ('b', 'Streaming'),
    ('c', 'Dancing'),
    ('d', 'Parking'),
]

ans7 = [
    ('a', 'Menggunakan topi.'),
    ('b', 'Pembatasan fisik dan kebersihan tangan.'),
    ('c', 'Minum larutan oralit.'),
    ('d', 'Wajib menggunakan masker N95.'),
]

ans8 = [
    ('a', '911'),
    ('b', '081'),
    ('c', '119'),
    ('d', '021')
]

ans9 = [
    ('a', '1 hari saja'),
    ('b', '2 hari'),
    ('c', '30 hari'),
    ('d', '14 hari'),
]

ans10 = [
    ('a', 'Masker bedah'),
    ('b', 'Masker kain'),
    ('c', 'Masker buff'),
    ('d', 'Masker N95'),
]

class FormQuestion(forms.Form):

    question1 = forms.CharField(
        label='Kata ‘Corona’ berasal dari bahasa Latin yang memiliki arti?', 
        widget=forms.RadioSelect(choices=ans1))

    question2 = forms.CharField(
        label='Berikut ini merupakan cara mencegah penyebaran Covid 19, kecuali.', 
        widget=forms.RadioSelect(choices=ans2))

    question3 = forms.CharField(
        label='Diwajibkan untuk menggunakan masker, saat...', 
        widget=forms.RadioSelect(choices=ans3))

    question4 = forms.CharField(
        label='Saat berobat atau memeriksakan diri ke fasilitas pelayanan kesehatan, Anda harus melakukan beberapa hal dibawah ini, kecuali.', 
        widget=forms.RadioSelect(choices=ans4))  

    question5 = forms.CharField(
        label='Jika seseorang kriteria Covid-19, maka akan dilakukan tahapan berikut, kecuali.', 
        widget=forms.RadioSelect(choices=ans5))  
    
    question6 = forms.CharField(
        label='Ketika menjalani pemeriksaan di pelayanan kesehatan, petugas kesehatan akan melakukan...', 
        widget=forms.RadioSelect(choices=ans6))  

    question7 = forms.CharField(
        label='Mengenakan masker saja tidak cukup untuk melindungi diri dari COVID-19, sehingga harus dikombinasikan dengan...', 
        widget=forms.RadioSelect(choices=ans7))   
    
    question8 = forms.CharField(
        label='Masyarakat bisa mengakses layanan ... untuk laporan gawat darurat masalah kesehatan.', 
        widget=forms.RadioSelect(choices=ans8))  

    question9 = forms.CharField(
        label='Masa inkubasi COVID-19 diperkirakan antara 1 sampai...', 
        widget=forms.RadioSelect(choices=ans9))  

    question10 = forms.CharField(
        label='Tiga jenis masker yang direkomendasikan oleh Kementerian Kesehatan, yakni, kecuali.', 
        widget=forms.RadioSelect(choices=ans10))  

    
class RequestForm(forms.ModelForm):
    class Meta:
        model = RequestQues
        fields = ['title']

        widget = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'style': "width: 100%"})
        }
