from django.test import TestCase
from django.test import Client 
from django.urls import resolve, reverse
from .models import Questions, RequestQues
from .forms import FormQuestion
from .views import welcom, quiz, show
from django.apps import apps
from .apps import QuizConfig
from django.contrib.auth.models import User


# Create your tests here.

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(QuizConfig.name, 'quiz') 
        self.assertEqual(apps.get_app_config('quiz').name, 'quiz')



class ModelTest(TestCase):
    def setUp(self):
        self.RequestQues = RequestQues.objects.create(title="what?")
        self.Question = Questions.objects.create(
            question="what?", option1 ="a", option2="b", option3="c", option4="d", answer="a")
        User.objects.create_user(
            'username', email='email@email.com', password='password')
        self.client.login(username='username', password='password')
    
    def test_model_dibuat(self):
        self.assertEqual(Questions.objects.count(), 1)
        self.assertEqual(RequestQues.objects.count(), 1)

    def test_str(self):
        self.assertEquals(str(self.RequestQues), "what?")
        # self.assertEquals(str(self.question), "what?")


class FormTest(TestCase):
    def setUp(self):
        self.RequestQues = RequestQues.objects.create(title="what?")
        self.Question = Questions.objects.create(
            question="what?", option1 ="a", option2="b", option3="c", option4="d", answer="a")
        User.objects.create_user(
            'username', email='email@email.com', password='password')
        self.client.login(username='username', password='password')

    def test_form_invalid(self):
        form = FormQuestion(data={})
        self.assertFalse(form.is_valid())

class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    # def test_url_welcome_ada(self):
    #     response = Client().get('/quiz')
    #     self.assertEquals(response.status_code, 301)
    
    # def test_url_kuis_ada(self):
    #     response = Client().get('/quiz/quiz/')
    #     self.assertEquals(response.status_code, 302)
    
    # def test_url_show_ada(self):
    #     response = Client().get('/quiz/showAnswer/')
    #     self.assertEquals(response.status_code, 302)
    
    # def test_url_requestQuesComplete_ada(self):
    #     response = self.client.get('quiz/request/(?P<id>[^/]+)/completed/$')
    #     self.assertEquals(response.status_code, 200)
    
    # def test_url_requestQuesDelete_ada(self):
    #     response = self.client.get('/quiz/request/1/delete/')
    #     self.assertEquals(response.status_code, 200)

    def test_url_requestQues_ada(self):
        response_get = Client().get("/quiz/request/", {"title": "why?"})
        self.assertEqual(response_get.status_code, 200)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.welcom = reverse("quiz:welcom")
        self.quiz = reverse("quiz:quiz")
        self.show = reverse("quiz:show")
        self.req = reverse("quiz:req_list_url")
        # self.complete = reverse("quiz:req_complete_url")
        # self.delete = reverse("quiz:req_delete_url")
        # self.complete = reverse('quiz/request/(?P<id>[^/]+)/completed/$')
        User.objects.create_user(
            'username', email='email@email.com', password='password')
        self.client.login(username='username', password='password')


    def test_GET_welcom(self):
        response = self.client.get(self.welcom)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'quiz/welcom.html')

    def test_GET_quiz(self):
        response = self.client.get(self.quiz, {'question': 'what?', 'option1':'a', 'option2':'b', 'option3':'c', 'option4':'d', 'answer':'a'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'quiz/quiz.html')
        
    def test_views_show(self):
        response = self.client.get(self.show)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'quiz/show.html')

    def test_views_req(self):
        response = self.client.get(self.req, {'title': 'what?'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'quiz/request.html')

    # def test_url_requestQuesComplete_ada(self): 
    #     'quiz/request/(?P<id>[^/]+)/completed/$'
    #     response = self.client.get(self.complete)

    #     # response = self.client.get('/quiz/request/1/completed/')
    #     self.assertEquals(response.status_code, 405)
    #     self.assertTemplateUsed(response, 'quiz/request.html')

    
    # def test_url_requestQuesDelete_ada(self):
    #     response = self.client.get(self.delete)

    #     # response = self.client.get('/quiz/request/1/delete/')
    #     self.assertEquals(response.status_code, 405)
    #     self.assertTemplateUsed(response, 'quiz/request.html')
