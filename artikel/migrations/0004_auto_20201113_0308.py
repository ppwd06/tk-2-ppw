# Generated by Django 3.1.2 on 2020-11-13 03:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('artikel', '0003_auto_20201113_0253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artikel',
            name='author',
            field=models.CharField(max_length=25),
        ),
        migrations.AlterField(
            model_name='artikel',
            name='category',
            field=models.CharField(choices=[('Berita', 'Berita'), ('Teknologi', 'Teknologi'), ('Hobi', 'Hobi'), ('Kesehatan', 'Kesehatan')], default='Berita', max_length=10),
        ),
        migrations.AlterField(
            model_name='artikel',
            name='title',
            field=models.CharField(max_length=100),
        ),
    ]
