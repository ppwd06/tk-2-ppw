from django.shortcuts import render, redirect
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.utils import formats

import json
import urllib.request

from .forms import ArtikelForm
from .models import Artikel


@login_required
def data_index(request):
    list_artikel = Artikel.objects.all()[::-1]
    # page = request.GET.get('page', 1)

    # paginator = Paginator(list_artikel, 5)
    # try:
    #     artikel = paginator.page(page)
    # except PageNotAnInteger:
    #     artikel = paginator.page(1)
    # except EmptyPage:
    #     artikel = paginator.page(paginator.num_pages)

    # context = {
    #     'title': 'Artikel',
    #     'list_artikel': artikel
    # }

    # return render(request, 'artikel/index.html', context)

    for artikel in list_artikel:
        artikel.author_username = artikel.author.username
        artikel.date = formats.date_format(
            artikel.date_posted, "DATETIME_FORMAT")
        artikel.save()

    post_list = serializers.serialize('json', list_artikel)
    return HttpResponse(post_list, content_type="text/json-comment-filtered")


@login_required
def index(request):

    context = {
        'title': 'Artikel',
    }

    return render(request, 'artikel/index.html', context)


@login_required
def forms(request):
    artikel_form = ArtikelForm(request.POST or None)
    if request.method == 'POST':
        if artikel_form.is_valid():
            artikel = artikel_form.save(commit=False)
            artikel.author = request.user
            artikel.save()
        artikel_form.save()

        return redirect('artikel:index')

    context = {
        'title': 'Buat Artikel',
        'artikel_form': artikel_form
    }

    return render(request, 'artikel/forms.html', context)


@login_required
def detail(request, slug):
    artikel = Artikel.objects.get(slug=slug)

    context = {
        'title': artikel.title,
        'artikel': artikel
    }

    return render(request, 'artikel/detail.html', context)


@login_required
def category(request, category):
    list_artikel = Artikel.objects.filter(category=category)[::-1]
    page = request.GET.get('page', 1)

    paginator = Paginator(list_artikel, 5)
    try:
        artikel = paginator.page(page)
    except PageNotAnInteger:
        artikel = paginator.page(1)
    except EmptyPage:
        artikel = paginator.page(paginator.num_pages)

    context = {
        'title': category,
        'list_artikel': artikel
    }

    return render(request, 'artikel/category.html', context)


@login_required
def delete(request, id):
    artikel = Artikel.objects.get(id=id)

    if request.user == artikel.author:
        artikel.delete()
        return redirect('artikel:index')
    else:
        return render(request, 'artikel/gandalf.html')


@login_required
def update(request, id):
    artikel_update = Artikel.objects.get(id=id)

    if request.user == artikel_update.author:
        data = {
            'title': artikel_update.title,
            'content': artikel_update.content,
            'category': artikel_update.category,
        }

        artikel_form = ArtikelForm(request.POST or None,
                                   initial=data, instance=artikel_update)

        if request.method == 'POST':
            if artikel_form.is_valid():
                artikel_form.save()

                return redirect('artikel:index')

        context = {
            'title': 'Form',
            'artikel_form': artikel_form
        }

        return render(request, 'artikel/forms.html', context)
    else:
        return render(request, 'artikel/gandalf.html')


def data(request):

    url_news = "https://www.news.developeridn.com/"
    connection = urllib.request.urlopen(url_news)
    js = connection.read()
    info = json.loads(js.decode("utf-8"))

    return JsonResponse(info)


def index2(request):

    context = {
        'title': 'Artikel',
    }

    return render(request, 'artikel/index2.html', context)


def data_detail(request):

    url = request.GET.get(
        'url', 'https://www.cnnindonesia.com/internasional/20200513095240-134-502769/turis-jatuh-usai-nekat-kunjungi-yellowstone-saat-pandemi')

    url_news = "https://www.news.developeridn.com/detail/?url="
    connection = urllib.request.urlopen(url_news + url)
    js = connection.read()
    info = json.loads(js.decode("utf-8"))

    return JsonResponse(info)


def detail2(request):

    url = request.GET.get(
        'url', 'https://www.cnnindonesia.com/internasional/20200513095240-134-502769/turis-jatuh-usai-nekat-kunjungi-yellowstone-saat-pandemi')

    context = {
        'title': 'Artikel',
        'url': url
    }

    return render(request, 'artikel/detail2.html', context)


def data_search(request):

    q = request.GET.get(
        'q', 'indonesia')

    url_news = "https://www.news.developeridn.com/search/?q="
    connection = urllib.request.urlopen(url_news + q)
    js = connection.read()
    info = json.loads(js.decode("utf-8"))

    return JsonResponse(info)


def search(request):

    q = request.GET.get(
        'q', 'indonesia')

    q = 'indonesia' if q == '' else q

    context = {
        'title': q,
        'q': q
    }

    return render(request, 'artikel/index2.html', context)
