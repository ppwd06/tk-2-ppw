$(document).ready(function () {
    $.ajax({
        url: `/artikel/data-detail?url=${url}`,
        success: function (json) {
            let content = ""
            if ('message' in json.data[0]) {
                content += '<div class="media-body"><div class="article-metadata mb-2">'
                content += '<h1>PAGE NOT FOUND !!!</h1></div>'
                content += '<a class="btn btn-primary rounded-pill" href="/artikel/umum">'
                content += 'Go Back Home</a></div>'
            }

            else {
                for (let berita in json.data) {
                
                    content += '<div class="media-body"><div class="article-metadata">'
                    content += `<h1 class="font-weight-bold mb-5 mt-2">${json.data[berita].judul}</h1>`
                    content += `<img class="mb-3 img-fluid rounded" src="${json.data[berita].poster}">`
                    content += `<p class="article-content">${json.data[berita].body}</p>`
                    content += '</div></div>'
                }
            }
            $("#detail-berita").append(content)
        }
    });
});
