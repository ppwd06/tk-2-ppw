$(document).ready(function () {
    $.ajax({
        url: '/artikel/data/',
        success: function (json) {
            let content = ""
            for (let berita in json.data) {
                
                content += '<article class="media content-section"><div class="media-body"><div class="article-metadata">'
                content += `<span class="mr-2" onclick='search(${json.data[berita].tipe})'>${json.data[berita].tipe}</span>`
                content += `<small class="text-muted">${json.data[berita].waktu}</small></div>`
                content += '<h2 class="title">'
                content += `<a class="title" href="/artikel/detail2?url=${json.data[berita].link}">${json.data[berita].judul}</a></h2>`
                content += `<img src="${json.data[berita].poster}" class="img-fluid rounded">`
                content += '</div></article>'
            }
            $("#berita").append(content)
        }
    });
    $("#searchLup").click(function () {
        search($("#searchBar").val());
    });
    $('#searchBar').keypress(function (e) {
        var key = e.which;
        if(key == 13) {
            search($("#searchBar").val());
        }
    });
});

function search(parameter) {
    window.location.href = `/artikel/search?q=${parameter}`;
}