$(document).ready(function () {
    $.ajax({
        url: '/artikel/data-index/',
        success: function (json) {
            console.log(json);
            let content = ""
            for (i = 0; i < json.length; i++) {
                console.log(json[i].fields);
                console.log(json[i].fields.author.username);
                content += `<article class="media content-section"><div class="media-body"><div class="article-metadata">`
                content += `<span class="mr-2">${json[i].fields.author_username}</span><small class="text-muted">${json[i].fields.date}</small>`
                content += `</div><h2 class="title"><a class="title" href="/artikel/detail/${json[i].fields.slug}">${json[i].fields.title}</a>`
                content += `</h2><p class="article-content">${json[i].fields.content}</p><a class="article-category"`
                content += `href="/artikel/category/${json[i].fields.category}">(${json[i].fields.category})</a></div></article>`
            }
            $("#artikel").append(content)
        }
    });
});
