from django import forms

from .models import Artikel


class ArtikelForm(forms.ModelForm):

    class Meta:
        model = Artikel
        fields = [
            'title',
            'content',
            'category',
        ]
        labels = {
            'title': 'Judul :',
            'content': 'Konten :',
            'category': 'Kategori :',
        }
        widgets = {
            'title': forms.TextInput(
                attrs={
                    'class': 'form-control form-section',
                    'placeholder': 'Judul Artikel'
                }
            ),
            'content': forms.Textarea(
                attrs={
                    'class': 'form-control form-section',
                    'placeholder': 'Konten Artikel'
                }
            ),
            'category': forms.Select(
                attrs={
                    'class': 'form-control form-section'
                }
            ),
        }
