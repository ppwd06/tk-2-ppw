from django.http import HttpRequest, response
from django.contrib.auth.models import User
from django.contrib.sessions.backends.db import SessionStore
from django.test import TestCase
from django.urls import reverse, resolve

from .views import *
from .models import Artikel

# Create your tests here.


class UmumTestCase(TestCase):

    # test index umum dan json
    def test_url_umum_is_exist(self):
        response = self.client.get('/artikel/umum/')
        self.assertEqual(response.status_code, 200)

    def test_url_umum_fires_correct_views_method(self):
        response = resolve('/artikel/umum/')
        self.assertEqual(response.func, index2)

    def test_views_umum_render_correct_html(self):
        response = self.client.get('/artikel/umum/')
        self.assertTemplateUsed(response, 'artikel/index2.html')

    def test_json_data(self):
        response = self.client.get('/artikel/data/')
        self.assertEqual(response.status_code, 200)

    def test_url_data_fires_correct_views_method(self):
        response = resolve('/artikel/data/')
        self.assertEqual(response.func, data)

    # test detail umum dan json
    def test_url_detail2_is_exist(self):
        response = self.client.get('/artikel/detail2/')
        self.assertEqual(response.status_code, 200)

    def test_url_detail2_fires_correct_views_method(self):
        response = resolve('/artikel/detail2/')
        self.assertEqual(response.func, detail2)

    def test_views_detail2_render_correct_html(self):
        response = self.client.get('/artikel/detail2/')
        self.assertTemplateUsed(response, 'artikel/detail2.html')

    def test_json_data_detail(self):
        response = self.client.get('/artikel/data-detail/')
        self.assertEqual(response.status_code, 200)

    def test_url_data_detail_fires_correct_views_method(self):
        response = resolve('/artikel/data-detail/')
        self.assertEqual(response.func, data_detail)

    # test category umum dan json
    def test_url_search_is_exist(self):
        response = self.client.get('/artikel/search/')
        self.assertEqual(response.status_code, 200)

    def test_url_search_fires_correct_views_method(self):
        response = resolve('/artikel/search/')
        self.assertEqual(response.func, search)

    def test_views_search_render_correct_html(self):
        response = self.client.get('/artikel/search/')
        self.assertTemplateUsed(response, 'artikel/index2.html')

    def test_json_data_search(self):
        response = self.client.get('/artikel/data-search/')
        self.assertEqual(response.status_code, 200)

    def test_url_data_search_fires_correct_views_method(self):
        response = resolve('/artikel/data-search/')
        self.assertEqual(response.func, data_search)


class UserTestCase(TestCase):

    # Init
    def setUp(self):
        User.objects.create_user(
            'username', email='email@email.com', password='password')

    # test index
    def test_index_url_response_if_user_is_not_logged_in(self):
        response = self.client.get('/artikel/')
        self.assertEqual(response.status_code, 302)

    def test_index_url_response_if_user_is_logged_in(self):
        self.client.login(username='username', password='password')
        response = self.client.get('/artikel/')
        self.assertEqual(response.status_code, 200)

    def test_url_umum_fires_correct_views_method(self):
        self.client.login(username='username', password='password')
        response = resolve('/artikel/')
        self.assertEqual(response.func, index)

    def test_views_umum_render_correct_html(self):
        self.client.login(username='username', password='password')
        response = self.client.get('/artikel/')
        self.assertTemplateUsed(response, 'artikel/index.html')

    def test_json_data(self):
        self.client.login(username='username', password='password')
        response = self.client.get('/artikel/data-index/')
        self.assertEqual(response.status_code, 200)

    def test_url_data_fires_correct_views_method(self):
        self.client.login(username='username', password='password')
        response = resolve('/artikel/data-index/')
        self.assertEqual(response.func, data_index)

    # test models
    def test_can_create_a_kegiatan(self):
        user = User.objects.get(id=1)
        Artikel.objects.create(title="Data Science Keren", content='Keren Banget Parah',
                               author=user, category='Teknologi')
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 1)

        artikel_data_science = Artikel.objects.get(id=1)
        self.assertEqual(str(artikel_data_science),
                         'Data Science Keren - username')

    # test form
    def test_can_save_a_POST_artikel_request(self):
        self.client.login(username='username', password='password')
        response = self.client.post('/artikel/buat-artikel/', data={
                                    'title': "Data Science Keren", 'content': 'Keren Banget Parah', 'category': 'Teknologi'})
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/artikel/')

        # new_response = self.client.get('/artikel/')
        # html_response = new_response.content.decode('utf8')
        # self.assertIn('Data Science Keren', html_response)

        new_response_2 = resolve('/artikel/buat-artikel/')
        self.assertEqual(new_response_2.func, forms)

    def test_forms_url_status_200(self):
        self.client.login(username='username', password='password')
        response = self.client.get('/artikel/buat-artikel/')
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'artikel/forms.html')
        response = self.client.get(reverse('artikel:forms'))
        self.assertEqual(response.status_code, 200)

        new_response_2 = resolve('/artikel/buat-artikel/')
        self.assertEqual(new_response_2.func, forms)

    # test detail
    def test_detail_url(self):
        self.client.login(username='username', password='password')
        user = User.objects.get(id=1)
        artikel1 = Artikel.objects.create(
            title="Data Science Keren", content='Keren Banget Parah', author=user, category='Teknologi')
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 1)

        new_response = self.client.get('/artikel/detail/' + artikel1.slug)
        html_response = new_response.content.decode('utf8')
        self.assertIn('Keren Banget Parah', html_response)
        self.assertTemplateUsed(new_response, 'artikel/detail.html')

        new_response_2 = resolve('/artikel/detail/' + artikel1.slug)
        self.assertEqual(new_response_2.func, detail)

    # test category
    def test_category_url(self):
        self.client.login(username='username', password='password')
        user = User.objects.get(id=1)
        artikel1 = Artikel.objects.create(
            title="Data Science Keren", content='Keren Banget Parah', author=user, category='Teknologi')
        artikel2 = Artikel.objects.create(title="Data Science Academy Jadi Tiga Minggu",
                                          content='InsyaAllah bermanfaat dan seru', author=user, category='Berita')
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 2)

        new_response = self.client.get(
            '/artikel/category/' + artikel2.category)

        html_response = new_response.content.decode('utf8')
        self.assertIn('Data Science Academy Jadi Tiga Minggu', html_response)
        self.assertTemplateUsed(new_response, 'artikel/category.html')

        new_response_2 = resolve('/artikel/category/' + artikel2.category)
        self.assertEqual(new_response_2.func, category)

    # test delete
    def test_can_delete_a_artikel(self):
        self.client.login(username='username', password='password')
        user = User.objects.get(id=1)
        Artikel.objects.create(
            title="Data Science Keren", content='Keren Banget Parah', author=user, category='Teknologi')
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 1)

        new_response = resolve('/artikel/delete/1')
        self.assertEqual(new_response.func, delete)

        response = self.client.get('/artikel/delete/1')
        counting_remaining_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_remaining_available_artikel, 0)

    def test_cant_delete_a_artikel(self):
        self.client.login(username='username', password='password')
        User.objects.create_user(
            'username2', email='email@email.com', password='password')
        user2 = User.objects.get(id=2)
        Artikel.objects.create(
            title="Data Science Keren", content='Keren Banget Parah', author=user2, category='Teknologi')
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 1)

        new_response = resolve('/artikel/delete/1')
        self.assertEqual(new_response.func, delete)

        response = self.client.get('/artikel/delete/1')
        self.assertTemplateUsed(response, 'artikel/gandalf.html')

        counting_remaining_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_remaining_available_artikel, 1)

    # test update
    def test_can_update_a_POST_artikel_request(self):
        self.client.login(username='username', password='password')
        user = User.objects.get(id=1)
        Artikel.objects.create(
            title="Data Science Keren", content='Keren Banget Parah', author=user, category='Teknologi')
        response = self.client.post('/artikel/update/1', data={
                                    'title': "Data Science Melelahkan", 'content': 'Lelah', 'category': 'Teknologi'})
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 1)

        new_response = resolve('/artikel/update/1')
        self.assertEqual(new_response.func, update)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/artikel/')

        # new_response = self.client.get('/artikel/')
        # html_response = new_response.content.decode('utf8')
        # self.assertIn('Data Science Melelahkan', html_response)

    def test_cant_update_a_POST_artikel_request(self):
        self.client.login(username='username', password='password')
        User.objects.create_user(
            'username2', email='email@email.com', password='password')
        user2 = User.objects.get(id=2)
        Artikel.objects.create(
            title="Data Science Keren", content='Keren Banget Parah', author=user2, category='Teknologi')
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 1)

        new_response = resolve('/artikel/update/1')
        self.assertEqual(new_response.func, update)

        response = self.client.get('/artikel/update/1')
        self.assertTemplateUsed(response, 'artikel/gandalf.html')

        # new_response = self.client.get('/artikel/')
        # html_response = new_response.content.decode('utf8')
        # self.assertIn('Data Science Keren', html_response)
