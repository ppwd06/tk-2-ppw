[![pipeline status](https://gitlab.com/ppwd06/tk-2-ppw/badges/master/pipeline.svg)](https://gitlab.com/ppwd06/tk-2-ppw/-/commits/master)
[![coverage report](https://gitlab.com/ppwd06/tk-2-ppw/badges/master/coverage.svg)](https://gitlab.com/ppwd06/tk-2-ppw/-/commits/master)

Nama-nama Anggota Kelompok :

Naifathiya Langitadiva (1906299055)
Ferika Hafshah Zahira (1906298903)
Thoriq Aulia (1906307611)
Olivia Monica (1906350641)
Raihan Rabbaanii Rifai (1906305796)

Link Herokuapp : https://tkppwd06.herokuapp.com/

Cerita Aplikasi yang Diajukan Serta Kebermanfaatannya :

Aplikasi yang kami buat berbentuk blog, yang berisi berbagai fitur-fitur mengenai covid-19. Tujuan kami dalam pembuatan website ini adalah selain untuk memenuhi tugas kelompok PPW, tetapi juga untuk memberikan informasi serta menambah pengetahuan masyarakat mengenai pandemi covid-19 ini. Semua fitur yang kami buat memiliki kebermanfaatannya masing-masing.

Daftar Fitur yang Akan Diimplementasikan :

1. Homepage berisi navigasi yang mengarahkan ke fitur-fitur lainnya.
2. Artikel yang dipisahkan berdasarkan kategori artikel.
3. Form untuk user post yang akan ditampilkan hasil postnya.
4. Form untuk netizen comment suatu topik berkaitan dengan covid.
5. Quiz berdasarkan artikel tertentu yang telah kami buat.
6. Poll tentang aktivitas user selama #diRumahAja dan menampilkan hasil survey dari poll tersebut
