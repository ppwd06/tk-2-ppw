from django.db import models
from django.core import serializers
from django.contrib import auth

class Pesan(models.Model):

    nama = models.ForeignKey(
        auth.get_user_model(), 
        on_delete=models.CASCADE,
    )
    pesan = models.TextField()
    waktu = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        if len(self.pesan) <= 20:
            return "{}: {}".format(self.nama, self.pesan)
        else:
            return "{}: {}...".format(self.nama, self.pesan[0:21])