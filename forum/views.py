from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth.models import User
from .forms import FormPesan
from .models import Pesan
import json
def forum(request):
    if (request.method == 'POST'):
        form = FormPesan(request.POST or None)
        if form.is_valid():
            pesan = form.save(commit=False)
            pesan.nama = request.user
            pesan.save()
            return HttpResponseRedirect('/forum')
    else:
        form = FormPesan()
        post = Pesan.objects.all().order_by('-id')
        jumlah = Pesan.objects.count()
        context = { 'form':form, 'post':post, 'jumlah':jumlah, 'title':'Forum Sharing' }
        return render(request, 'post.html', context)

def getPost(request):
        post = list(Pesan.objects.values().order_by('-id'))
        return JsonResponse(post, safe=False)