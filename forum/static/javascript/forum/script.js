$(document).ready(function(){
    getPosts();
    
    $("form").submit(function(){
        if(confirm("Kirim pesan?")) {
            $.ajax({
                url:$(this).attr("action"),
                data:$(this).serialize(),
                type:"POST",
                success: function(result) {
                    console.log(result);
                }
            })
        } else {
            return false;
        }     
    })
})

function getPosts() {
    $.ajax({
        url: "getpost/",
        success: function(result){
            $("#posts").empty();
            
            $("#posts").append(
                '<div class=container>' +
                '<div class="row">' +
                '<div class="column">'
            )

            for (i = 0; i < result.length; i++) {
                if (i%2  == 0) {
                    
                    var nama = result[i].nama_id;
                    var waktu = result[i].waktu;
                    var pesan = result[i].pesan;
                    
                    $("#posts").append(
                    '<div class="card bg-post">' +
                        '<div class="card-title"><h4>' + nama + '</h4></div>' +
                        '<div class="card-time">Pada '+ waktu + '</div>' +
                        '<div class="card-content">' + pesan + '</div>' +
                    '</div>'
                    )
                }
            }
            
            $("#posts").append(
                '</div>' +
                '<div class="column">'
            )
            
            for (i = 0; i < result.length; i++) {
                if (i%2 == 1) {
                    
                    var nama = result[i].nama_id;
                    var waktu = result[i].waktu;
                    var pesan = result[i].pesan;
                    
                    $("#posts").append(
                    '<div class="card bg-post">' +
                        '<div class="card-title"><h4>' + nama + '</h4></div>' +
                        '<div class="card-time">Pada '+ waktu + '</div>' +
                        '<div class="card-content">' + pesan + '</div>' +
                    '</div>'
                    )
                }
            }
            
            $("#posts").append(
                '</div>' +
                '</div>' +
                '</div>'
            )
        }
    });
}