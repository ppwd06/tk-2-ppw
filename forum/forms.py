from django import forms
from .models import Pesan

class FormPesan(forms.ModelForm):
    
    class Meta:
        model = Pesan
        fields = ('pesan',)
        
        widgets = {
            'nama': forms.TextInput(
                attrs={'class': 'form-control content-section',}
            ),
            'pesan': forms.Textarea(
                attrs={'class': 'form-control content-section',}
            )
        }