from django.shortcuts import render
from .models import Topik, Komentar
from .forms import FormKomentar
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, auth
from django.http import JsonResponse
from django.core import serializers


def welcome(request):
    return render(request, 'welcome.html', {'title': 'Chitchat'})

@login_required
def topik(request):
    alltopik = Topik.objects.all()
    responses = {
        'title': 'Chitchat - Topik',
        'alltopik' : alltopik,
    }
    return render(request, 'topik.html', responses)


def cari_topik(request):
    keyword = request.GET.get('q',"")

    datatemp = []
    if keyword != "":
        for isi in Topik.objects.all():
            if keyword in isi.nama: 
                item = {'topik' : isi.nama, 'key': isi.id}
                datatemp.append(item)
    else:
        for isi in Topik.objects.all():
            item = {'topik' : isi.nama}
            datatemp.append(item)

    return JsonResponse({'data':datatemp})

@login_required
def forum(request,id,name):
    topik = Topik.objects.get(id=id)
    komentar = Komentar.objects.filter(topik__nama = topik.nama)

    form = FormKomentar(initial= {'topik': id, 'nama': name,})
    responses = {
        'komentar': komentar,
        'title': 'Chitchat - Forum',
        'topik' : topik,
        'nama' : name,
        'form' : form,
    }
    return render(request, 'forum.html', responses)

@login_required
def komentar(request,id,name):
    topik = Topik.objects.get(id=id)
    datatemp = []
    if request.POST:
        komentar = request.POST.get('komentar')
        topik_id = request.POST.get('topik')
        nama = request.POST.get('nama')
        Komentar.objects.create(
            komentar = komentar,
            topik = Topik.objects.get(id=topik_id),
            nama = nama,
            )
        for isi in Komentar.objects.filter(topik__nama = topik.nama):
            item = {'nama':isi.nama, 'komentar':isi.komentar, 'waktu': isi.waktu}
            datatemp.append(item)
        return JsonResponse({'data':datatemp})

