from django.urls import include, path
from chitChat import views

urlpatterns = [
	path('', views.welcome, name='welcome'),
	path('topik/', views.topik, name='topik'),
	path('topik/caritopik/', views.cari_topik, name='caritopik'),
	path('forum/<int:id>/<str:name>/', views.forum, name='forum'),
	path('forum/<int:id>/<str:name>/komentar/', views.komentar, name='forum'),
]