from django.contrib import admin
from .models import Komentar, Topik

class KomentarAdmin(admin.ModelAdmin):
    list_display = [ 'topik', 'nama']
    search_fields = ['topik', 'nama']
    list_filter = ('topik', 'nama')
    list_per_page = 10

admin.site.register(Topik)
admin.site.register(Komentar)
