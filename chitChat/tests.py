from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Komentar, Topik
from .views import welcome, topik, forum
from django.contrib.auth.models import User, auth
from django.contrib.auth import authenticate, login, logout

# Create your tests here.

# URL TEST
class TestTKPPW(TestCase):
    
    def setUp(self):
        User.objects.create_user(
            'budi', email='email@email.com', password='admin123')

        self.bahasan = Topik.objects.create(
            nama = 'tester topik',
            id=1
        )

        self.komen = Komentar.objects.create(
            nama = 'budi',
            komentar = 'ini bapak budi',
            topik = self.bahasan,
            id=1
        )


# Test URL
    def test_apakah_url_chitchat_ada(self):
        client = Client()
        response = client.get('/chitchat/')
        self.assertEquals(response.status_code, 200)

    def test_apakah_url_topik_noLogin_ada(self):
        client = Client()
        response = client.get('/chitchat/topik/')
        self.assertEquals(response.status_code, 302)
        
    def test_apakah_url_forum_noLogin_ada(self):
        client = Client()
        response = client.get('/chitchat/forum/1/budi/')
        self.assertEquals(response.status_code, 302)
    
    def test_apakah_url_komentar_noLogin_ada(self):
        client = Client()
        response = client.get('/chitchat/forum/1/budi/komentar/')
        self.assertEquals(response.status_code, 302)


    def test_apakah_url_topik_Login_ada(self):
        client = Client()
        did_login_succeed = self.client.login(
            username="budi",
            password="admin123")
        self.assertTrue(did_login_succeed)
        response = self.client.get('/chitchat/topik/')
        
    def test_apakah_url_forum_Login_ada(self):
        self.client.login(username='budi', password='admin123')
        response = self.client.get('/chitchat/forum/1/budi/')
        self.assertEqual(response.status_code, 200)
    
    def test_view_komentar_(self):
        self.client.login(username='budi', password='admin123')
        response = self.client.post(
            "/chitchat/forum/1/budi/komentar/", data={
                "komentar":"test",
                "topik":"1",
                "nama":"budi",
            }
        )
        self.assertEqual(response.status_code, 200)

    def test_view_cariTopik(self):
        self.client.login(username='budi', password='admin123')
        response = self.client.get("/chitchat/topik/caritopik/?q=tester")
        self.assertEqual(response.status_code, 200)
   
# Test View   
    def test_view_welcome(self):
        url = '/chitchat/'
        response = Client().get(url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'welcome.html')

    def test_view_topik(self):
        self.client.login(username='budi', password='admin123')
        response = self.client.get('/chitchat/topik/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'navbar.html')
        self.assertTemplateUsed(response, 'topik.html')

    def test_view_forum(self):
        self.client.login(username='budi', password='admin123')
        response = self.client.get('/chitchat/forum/1/budi/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'navbar.html')
        self.assertTemplateUsed(response, 'forum.html')

    def test_view_cariTopik(self):
        self.client.login(username='budi', password='admin123')
        response = self.client.get("/chitchat/topik/caritopik/?q=tester")
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'data': [{'topik' : 'tester topik', 'key': 1}]}
        )



#Model test
    def test_Topik(self):
        testTopik = Topik.objects.get(id=1)
        self.assertEqual(testTopik.nama, "tester topik")
        self.assertEqual(str(testTopik), "tester topik")

    def test_peserta(self):
        testkomentar = Komentar.objects.get(id=1)
        self.assertEqual(str(testkomentar), "budi")
        self.assertEqual(testkomentar.nama,"budi")
        self.assertEqual(testkomentar.komentar,"ini bapak budi")
        self.assertEqual(testkomentar.topik, Topik.objects.get(id=1))
