from django import forms
from django.forms import ModelForm
from .models import Komentar

class FormKomentar(ModelForm):
    class Meta:
        model = Komentar
        fields = ['nama','komentar','topik']
    error_messages = {
        'required' : 'Please Type'
    }
    komentar = forms.CharField( label='komentar ',  
                                max_length=100, 
                                required=True,
                                widget=forms.TextInput(attrs={'placeholder': 'enter your comment'}),)