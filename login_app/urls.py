from django.urls import path

from . import views
from django.contrib.auth import views as auth_views

app_name = 'login_app'

urlpatterns = [
    path('register/', views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='login_app/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='login_app/logout.html'), name='logout'),
]
