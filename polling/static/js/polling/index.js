$(document).ready(function () {
    $("body").on({
        dblclick: function(){
            $(".navbar-custom").css("background-color", "#508D7D");
            $(this).css("background-color", "#EA98AE");
            $(this).css("color", "#F3F3DB");
        },  
       
        mouseleave: function(){
            $(".navbar-custom").css("background-color", "#EA98AE");
            $(this).css("background-color", "#F3F3DB");
            $(this).css("color", "#444444");
        },
       
    });
});