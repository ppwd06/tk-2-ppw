$(document).ready(function () {
    $("body").on({
        mouseenter: function(){
            $(".navbar-custom").css("background-color", "#508D7D");
            $(this).css("background-color", "#EA98AE");
            $(this).css("color", "#F3F3DB");
        },  
       
        mouseleave: function(){
            $(".navbar-custom").css("background-color", "#EA98AE");
            $(this).css("background-color", "#F3F3DB");
            $(this).css("color", "#444444");
        },

        dblclick: function(){
            $(".navbar-custom").css("background-color", "#EA98AE");
            $(this).css("background-color", "#F3F3DB");
            $(this).css("color", "#444444");
        },
       
    });

    $(".panel").hide();
    $(".panel2").hide();
    $(".panel3").hide();
    
    $.ajax({
        url: '/polling/film-recommendation/',
        // url: 'https://api.themoviedb.org/3/tv/popular?api_key=b70f627824fe3608d10d7e5a0b53b517&language=en-US&page=1',
        success: function(data) {
            var array_items = data.results;
            console.log(array_items);
            $("#daftar_isi").empty();
            for (i = 1; i < 11; i++) {
                var no = i;
                var judul = array_items[i].name;
                var gambar = 'https://image.tmdb.org/t/p/w200' + array_items[i].poster_path;
                var tahun = array_items[i].first_air_date;
                $('#daftar_isi').append("<tr><td>" + no + "</td><td><img src=" + gambar + "></td><td>" + judul + "</td><td>" + tahun + "</td></tr>")
            }
        }
    });

    $("#flip").mouseenter(function(){
        $(".panel").slideDown("slow");
    });
    $("#flip").mouseleave(function(){
        $(".panel").slideUp("slow");
    });

    $("#flip2").mouseenter(function(){
        $(".panel2").slideDown("slow");
    });
    $("#flip2").mouseleave(function(){
        $(".panel2").slideUp("slow");
    });

    $("#flip3").mouseenter(function(){
        $(".panel3").slideDown("slow");
    });
    $("#flip3").mouseleave(function(){
        $(".panel3").slideUp("slow");
    });
});