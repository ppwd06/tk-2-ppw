from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
import requests
import json

from .forms import PollForm
from .models import PollModel

# Create your views here.
def index(request):
    context = {
        'title' : 'Poll'
    }
    return render(request, 'polling/index.html', context)

# @login_required
def poll(request):
    form = PollForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        cd = form.cleaned_data
        PollModel.objects.create(activity = cd['activity'])
        # form.save()
        return redirect('polling:result')
    context = {
        'title' : 'Poll',
        'form': form
    }
    return render(request, 'polling/poll.html', context)

# @login_required
def result(request):
    option1 = PollModel.objects.filter(activity='olahraga').count()
    option2 = PollModel.objects.filter(activity='bacabuku').count()
    option3 = PollModel.objects.filter(activity='bingewatch').count()
    option4 = PollModel.objects.filter(activity='kuliah').count()
    options = [option1, option2, option3, option4]
    maks = max(options)
    
    context = {
        'title' : 'Poll',
        'option1': option1,
        'option2' : option2,
        'option3' : option3,
        'option4' : option4,
        'maks' : maks,
    }

    return render(request, 'polling/result.html', context)

def film_rec(request):
    url = "https://api.themoviedb.org/3/tv/popular?api_key=b70f627824fe3608d10d7e5a0b53b517&language=en-US&page=1"
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)