from django import forms
from .models import PollModel

act_choices = [
    ('olahraga', 'Olahraga'),
    ('bacabuku', 'Baca Buku'),
    ('bingewatch', 'Binge Watching'),
    ('kuliah', 'Sibuk Kuliah'),
]

class PollForm(forms.ModelForm):
    class Meta:
        model = PollModel
        fields = ['activity']

    activity = forms.CharField(
        widget = forms.RadioSelect(choices=act_choices)
        )