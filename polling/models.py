from django.db import models

act_choices = [
    ('olahraga', 'Olahraga'),
    ('bacabuku', 'Baca Buku'),
    ('bingewatch', 'Binge Watching'),
    ('kuliah', 'Sibuk Kuliah'),
]

# Create your models here.
class PollModel(models.Model):
    activity = models.CharField(
        max_length=30,
        choices=act_choices
    )

    def __str__(self):
        return self.activity
