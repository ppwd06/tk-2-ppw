from django.urls import include, path
from . import views

app_name = 'polling'

urlpatterns = [
    path('film-recommendation/', views.film_rec, name='film_rec'),
    path('poll-result/', views.result, name='result'),
    path('poll/', views.poll, name='poll'),
	path('', views.index, name='index'),
]